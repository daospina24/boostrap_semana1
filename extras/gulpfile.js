'use strict'

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    broserSync = require('browser-sync')
const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-uglify');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatMap = require('gulp-flatmap');
const htmlMin = require('gulp-htmlmin');

gulp.task('sass', function() {
    gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'))
});

gulp.task('sass:watch', function() {
    gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('browser-sync', function() {
    var files = ['./*.html', './css/*.css', './images/*.{png, jpg, jpeg, gif}', './js/*.js']
    broserSync.init(files, {
        server: {
            baseDir: './'
        }
    });

});


gulp.task('clean', function() {
    return del(['dist'])

})

gulp.task('copyfonts', function() {
    gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
        .pipe(gulp.dest('./dist/fonts'))

})

gulp.task('imagemin', function() {
    return gulp.src('./images/*.{png, jpg, jpeg, gif}')
        .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
        .pipe(gulp.dest('dist/images'))

})
gulp.task('usemin', function() {
    return gulp.src('./*.html')
        .pipe(flatMap(function(stream, file) {
            return stream
                .pipe(usemin({
                    css: [rev()],
                    html: [function() { return htmlMin({ collapseWhitespace: true }) }],
                    js: [uglify(), rev()],
                    inlinejs: [uglify()],
                    inlinecss: [cleanCss(), 'concat']
                }))

        }))
        .pipe(gulp.dest('dist/'))

})

gulp.task('build', ['clean'], function() {
    gulp.start('copyfonts', 'imagemin', 'usemin')

})