$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 2000
    });
    $('#contacto').on('show.bs.modal', function(e) {
        console.log('El modal de contacto se esta mostrando')

        $('#btnContacto1').removeClass('btn-outline-success');
        $('#btnContacto1').addClass('btn-primary');
        $('#btnContacto1').prop('disabled', true);

        $('#btnContacto2').removeClass('btn-outline-success');
        $('#btnContacto2').addClass('btn-primary');
        $('#btnContacto2').prop('disabled', true);

        $('#btnContacto3').removeClass('btn-outline-success');
        $('#btnContacto3').addClass('btn-primary');
        $('#btnContacto3').prop('disabled', true);

        $('#btnContacto4').removeClass('btn-outline-success');
        $('#btnContacto4').addClass('btn-primary');
        $('#btnContacto4').prop('disabled', true);

        $('#btnContacto5').removeClass('btn-outline-success');
        $('#btnContacto5').addClass('btn-primary');
        $('#btnContacto5').prop('disabled', true);



    });
    $('#contacto').on('hide.bs.modal', function(e) {
        console.log('El modal de contacto se esta ocultando')

        $('#btnContacto1').removeClass('btn-primary');
        $('#btnContacto1').addClass('btn-outline-success');
        $('#btnContacto1').prop('disabled', false);

        $('#btnContacto2').removeClass('btn-primary');
        $('#btnContacto2').addClass('btn-outline-success');
        $('#btnContacto2').prop('disabled', false);

        $('#btnContacto3').removeClass('btn-primary');
        $('#btnContacto3').addClass('btn-outline-success');
        $('#btnContacto3').prop('disabled', false);

        $('#btnContacto4').removeClass('btn-primary');
        $('#btnContacto4').addClass('btn-outline-success');
        $('#btnContacto4').prop('disabled', false);

        $('#btnContacto5').removeClass('btn-primary');
        $('#btnContacto5').addClass('btn-outline-success');
        $('#btnContacto5').prop('disabled', false);


    })
});